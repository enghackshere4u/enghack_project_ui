import { StyleSheet } from 'react-native';

const paddingRight = 10;

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    chatContainer: {
      flex:1,
      //justifyContent: 'flex-end',
      //alignItems: 'flex-end',
    },
    captions: {
      flex:1,
      //justifyContent: 'center',
      //alignItems: 'center',
      borderWidth: 1, 
      borderColor: 'grey',
      borderRadius: 15, 
      padding: 15,
    }, 
    captionText:{
      fontSize: 14,
      padding:10,
    },
    buttonsContainer:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    bubble:{
      paddingVertical: 5,
      paddingHorizontal: paddingRight,
      borderWidth: 1, 
      borderColor: 'grey',
      borderRadius: 15, 
    },
    blueText: {
      color: 'blue'
    },
    whiteText: {
      color: 'white'
    }, 
    timeStamp: {
      color: 'grey', 
      fontSize: 10,
      paddingHorizontal: paddingRight
    }, 
    messageBubble: {
      padding: 5, 
      alignItems: 'flex-end',
    },
    conversationItem:{
      backgroundColor: 'white',
      padding: 15
    
    }, 
    row: {
      flex:1,
      flexDirection: 'row', 
      justifyContent: 'space-between'
    }, 
    chatScreenHeader: {
      textAlign: 'center',
      fontSize: 18,
      fontWeight: 'bold', 
      padding: 10
    }, 
    phoneButton: {
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.2)',
      alignItems:'center',
      justifyContent:'center',
      width:100,
      height:100,
      backgroundColor:'#fff',
      borderRadius:50,
    }
  });
  
  export default styles;