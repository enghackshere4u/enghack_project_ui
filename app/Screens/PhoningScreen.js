/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList, TextInput} from 'react-native';
import SoundRecordPlayer from '../SoundRecordPlayer';
import styles from '../styles';
import { restElement } from '@babel/types';
// import RNEventSource from 'react-native-event-source';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {USER_TO, USER_FROM} from "../GLOBAL";
import CaptionsController from '../Controller/CaptionsController';
// import AudioRecord from 'react-native-audio-record';
import {LabeledPicker} from '../Components/LabeledPicker';


const languages = [
  {
    label: "English",
    value: "en"
  },
  {
    label: "French",
    value: "fr"
  },
  {
    label: "Spanish",
    value: "es"

  },
  {
    label: "German",
    value: "de"
  },
  {
    label: "Chinese (Simplified)",
    value: "zh-cn"
  },
  {
    label: "Arabic",
    value: "ar"
  }
   
]


export default class PhoningScreen extends Component {

  constructor(props){
    super(props);
    this.soundRecordPlayer = new SoundRecordPlayer();
    this.captionsController = new CaptionsController();
    console.disableYellowBox=true;


    this.state = {
      messageLogs: [],
      selectedLanguage : languages[0],
    }

  }


  //   getNewLanguage() {
  //     let languageCode = this.state.selectedLanguage;
      

  //     if (languageCode != null) {
  //       console.log("language:", languageCode);
  //     }

  //     _this = this;
  //     if (languageCode != null) {
  //         fetch(`http://c5d9addc.ngrok.io/translate_message?language=${languageCode}&conversationID=${this.conversationID}`)
  //         .then(result => result.json())
  //         .then(result => {
  //           console.log("databade result", result);
  //           // _this.setState({
  //           //   messages: result.conversationHistory
  //           // });
  //           //this.captionsLogs = result;
            
  //       });
  //     }
    
  // }

  render() {
    return (
      <SafeAreaView style = {styles.container}>
        <ScrollView style={styles.container}>
        <LabeledPicker
            label='Language'
            placeholderText="Select Language for Translation"
            items={languages}
            value={this.state.selectedLanguage}
            onValueChange={(value) => this.setState({selectedLanguage: value})}
            //onDonePress={() => this.getNewLanguage()}
          />
        <View style={[styles.row, {padding: 15}]}>
          <TouchableOpacity
            style={styles.phoneButton}
            title="Call"
            onPress={() => this.onPressCall()}
            //onPress={() => this.onPressStartRecord()}
            color="green"
          >
            <Icon name = "phone" size={50} color="green"/>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.phoneButton}
            title="End"
            onPress={() => this.onPressHangUp()}
            //onPress={() => this.onPressStopRecord()}
            color="red"
          >
            <Icon name = "phone-hangup" size={50} color="red"/>
          </TouchableOpacity>
          {/* <Button
            title="End"
            onPress={() => this.onPressStopRecord()}
            color="red"
          /> */}
        </View>
        {/* <TextInput></TextInput> */}
        <View style={styles.captions}>
            {this.renderMessages()}
        </View>
        {/* <View style={styles.buttonsContainer}>
          <Button
            title="play"
            onPress={() => this.onPressStartPlay()}
          />
          <Button
            title="stop"
            onPress={() => this.onPressStopPlay()}
          />
          <Button
            title="googly"
            onPress={() => this.uploadFile()}
          />
          {/* <Button
            title="audio stuff"
            onPress={() => this.onPressAudioStuff()}
          /> */}
        {/* </View> */} 
        </ScrollView>
      </SafeAreaView>
    );
  }


  // onPressAudioStuff() {
  //   const options = {
  //     sampleRate: 16000,  // default 44100
  //     channels: 1,        // 1 or 2, default 1
  //     bitsPerSample: 16,  // 8 or 16, default 16
  //     audioSource: 6,     // android only (see below)
  //     wavFile: 'test.wav' // default 'audio.wav'
  //   };
     
  //   AudioRecord.init(options);
     
  //   AudioRecord.start();
     
  //   AudioRecord.stop();
  //   // or to get the wav file path
  //   //audioFile = await AudioRecord.stop();
     
  //   AudioRecord.on('data', data => {
  //     // base64-encoded audio data chunks
  //     console.log(data);
  //   });
  // }




  renderMessageItem = ({item}) => {
    console.log(item);
    let message = (!!item.translated_message) ? item.translated_message : item.message;

    return( 
      <Text style={styles.captionText}>{message}</Text>
      );
  }
 
  renderMessages() {
    console.log("message logs", this.state.messageLogs);
    return (
        <FlatList
          data={this.state.messageLogs}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderMessageItem}
        />
    );
  }



  // componentDidMount(){
  //   this._isMounted = true;
  //   this.load()
  //   this.props.navigation.addListener('willFocus', this.load)
  // }
  // load = () => {
  //   _this = this;
  //   this.captionsController.createCaptionsSubscription({
  //       callback: function(newMessages) {
  //         console.log("SHOULD BE ONLY MEssages", newMessages);
  //         if(_this._isMounted){
  //             console.log("made it in");
  //             let msg = newMessages.existence[0];
  //             if (!!msg) {
  //               _this.createAudioFile(msg);
  //             }
  //             if (newMessages.existence.length>0) {
  //               await delay(2500)
  //             }
  //             setTimeout(function () {
  //                 if (newMessages.existence.length>0) {
  //                     _this.setState({ messageLogs : newMessages.existence});
  //                 }
  //             }, 2500);
 
  //             _this.setState({ messageLogs : []});
              
  //         }
  //       }
  //     });  
  // }
  componentDidMount(){
    this._isMounted = true;
    //this.load()
    //this.props.navigation.addListener('willFocus', this.load)
  }


  // startListenening = () => {
  //   _this = this;
  //   this.captionsController.createCaptionsSubscription({
  //       language: this.state.selectedLanguage,
  //       callback: function(newMessages) {
  //         if(_this._isMounted){
  //             _this.setState({ messageLogs : newMessages.existence});
  //             let msg = newMessages.existence[0];
  //             if (!!msg) {
  //               _this.createAudioFile(msg);
  //             } 
  //         }
  //       }
  //     });  
  // }


  startListenening = () => {
    let language = (!!this.state.selectedLanguage) ? this.state.selectedLanguage : "";
    console.log(language);
    _this = this;
    this.captionsController.createCaptionsSubscription({
        language: language,
        callback: function(newMessages) {
          if(_this._isMounted){
              _this.setState({ messageLogs : newMessages.existence});
              let msg = newMessages.existence[0];
              if (!!msg) {
                _this.createAudioFile(msg);
                
              } 

              setTimeout(function () {
                  if (!!msg) {
                      _this.setState({ messageLogs : newMessages.existence});
                  }
              }, 1600);

                      
          }
        }
      });  
  }

  createAudioFile (item) {
    console.log("iteemmm", item);
    let base = item.messageBinary;
    this.soundRecordPlayer.playBase(base);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.captionsController.unsubscribe();
  }


  onPressCall() {
    this.startListenening();
    this.inCall = true;
    this.soundRecordPlayer.onStartRecord();
    //this.sendAudio();
    this.timer = setInterval(()=> this.sendAudio(), 3000);
  }


  // onPressCall() {
  //   this.soundRecordPlayer.onStartRecord({
  //     callback: ({recordSecs, recordTime}) => {
  //       //console.log(recordSecs, recordTime);
  //     }
  //   });
  //   this.timer = setInterval(()=> this.sendAudio(), 3000);
  // }

  sendAudio(){
    if (!this.inCall) { return }
    this.soundRecordPlayer.onStopRecord({
      callback: ({}) => {
        this.uploadFile();
        this.soundRecordPlayer.onStartRecord();
      }
    });
   
  }

  onPressHangUp () {
    this.inCall = false;
    this.soundRecordPlayer.onStopRecord({
      callback: ({}) => {
        this.timer = null;
      }
    });
  }

  // quicksendFile = async () => {

  //   var RNFS = require('react-native-fs');
  //   const path = this.soundRecordPlayer.getAudioFilePath();
  //   var content = await RNFS.readFile(path, 'base64');
  //   console.log(content);

  //   const uploadUrl = 'http://c5d9addc.ngrok.io/call_router';

  //   var x = fetch(uploadUrl, {
  //     method: 'POST',
  //     headers: {
  //       Accept: 'application/json',
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify({
  //       payload: content,
  //       conversationID: "4", 
  //       userFrom: USER_FROM,
  //       userTo: USER_TO
  //     }),
  //   });
  // }



  uploadFile = async () => {

    var RNFS = require('react-native-fs');
    const path = this.soundRecordPlayer.getAudioFilePath();
    var content = await RNFS.readFile(path, 'base64');
    // console.log(content);

    const uploadUrl = 'http://48ff42d1.ngrok.io/call_router';

    var x = fetch(uploadUrl, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        payload: content,
        conversationID: (USER_FROM==19) ? "Jamie" : "Jessica", 
        userFrom: USER_FROM,
        userTo: USER_TO
      }),
    })//.then((response) => console.log(response.json()));
  }

  // onPressStartRecord = () => {
  //   this.soundRecordPlayer.onStartRecord({
  //     callback: ({recordSecs, recordTime}) => {
  //       this.setState({
  //         recordSecs: recordSecs,
  //         recordTime: recordTime,
  //       });
  //     }
  //   });
  // }

  // onPressStopPlay = () => {
  //   this.soundRecordPlayer.onStopPlay();
  // }

  // onPressStartPlay = () => {
  //   this.soundRecordPlayer.onStartPlay({
  //     callback : ({currentPositionSec, currentDurationSec, playTime, duration}) => {
  //       this.setState({
  //         currentPositionSec: currentPositionSec,
  //         currentDurationSec: currentDurationSec,
  //         playTime: playTime,
  //         duration: duration,
  //       });
  //     }
  //   });
  // }

  // onPressStopRecord = () => {
  //   this.soundRecordPlayer.onStopRecord({
  //     callback : ({recordSecs}) => {
  //       this.setState({
  //         recordSecs: recordSecs,
  //       });
  //     }
  //   });
  // }




}















































// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, {Component} from 'react';
// import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList, TextInput} from 'react-native';
// import SoundRecordPlayer from '../SoundRecordPlayer';
// import styles from '../styles';
// import { restElement } from '@babel/types';
// // import RNEventSource from 'react-native-event-source';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import {USER_TO, USER_FROM} from "../GLOBAL";
// import CaptionsController from '../Controller/CaptionsController';
// // import AudioRecord from 'react-native-audio-record';


// export default class PhoningScreen extends Component {

//   constructor(props){
//     super(props);
//     this.soundRecordPlayer = new SoundRecordPlayer();
//     this.captionsController = new CaptionsController();

//     this.state = {
//       messageLogs: []
//     }

//   }

//   render() {
//     return (
//       <SafeAreaView style={styles.container}>
//         <View style={[styles.row, {padding: 15}]}>
//           <TouchableOpacity
//             style={styles.phoneButton}
//             title="Call"
//             onPress={() => this.onPressCall()}
//             //onPress={() => this.onPressStartRecord()}
//             color="green"
//           >
//             <Icon name = "phone" size={50} color="green"/>
//           </TouchableOpacity>
//           <TouchableOpacity
//             style={styles.phoneButton}
//             title="End"
//             onPress={() => this.onPressHangUp()}
//             //onPress={() => this.onPressStopRecord()}
//             color="red"
//           >
//             <Icon name = "phone-hangup" size={50} color="red"/>
//           </TouchableOpacity>
//           {/* <Button
//             title="End"
//             onPress={() => this.onPressStopRecord()}
//             color="red"
//           /> */}
//         </View>
//         {/* <TextInput></TextInput> */}
//         <View style={styles.captions}>
//             {this.renderMessages()}
//         </View>
//         <View style={styles.buttonsContainer}>
//           <Button
//             title="play"
//             onPress={() => this.onPressStartPlay()}
//           />
//           <Button
//             title="stop"
//             onPress={() => this.onPressStopPlay()}
//           />
//           <Button
//             title="googly"
//             onPress={() => this.uploadFile()}
//           />
//           {/* <Button
//             title="audio stuff"
//             onPress={() => this.onPressAudioStuff()}
//           /> */}
//         </View>
//       </SafeAreaView>
//     );
//   }


//   // onPressAudioStuff() {
//   //   const options = {
//   //     sampleRate: 16000,  // default 44100
//   //     channels: 1,        // 1 or 2, default 1
//   //     bitsPerSample: 16,  // 8 or 16, default 16
//   //     audioSource: 6,     // android only (see below)
//   //     wavFile: 'test.wav' // default 'audio.wav'
//   //   };
     
//   //   AudioRecord.init(options);
     
//   //   AudioRecord.start();
     
//   //   AudioRecord.stop();
//   //   // or to get the wav file path
//   //   //audioFile = await AudioRecord.stop();
     
//   //   AudioRecord.on('data', data => {
//   //     // base64-encoded audio data chunks
//   //     console.log(data);
//   //   });
//   // }




//   renderMessageItem = ({item}) => {
//     console.log(item);
//     return( 
//       <Text style={styles.captionText}>{item.message}</Text>
//       );
//   }
 
//   renderMessages() {
//     console.log("message logs", this.state.messageLogs);
//     return (
//         <FlatList
//           data={this.state.messageLogs}
//           keyExtractor={(item, index) => index.toString()}
//           renderItem={this.renderMessageItem}
//         />
//     );
//   }

//   componentDidMount(){
//     this._isMounted = true;
//     this.load()
//     this.props.navigation.addListener('willFocus', this.load)
//   }
//   load = () => {
//     _this = this;
//     this.captionsController.createCaptionsSubscription({
//         callback: function(newMessages) {
//           console.log("SHOULD BE ONLY MEssages", newMessages);
//           if(_this._isMounted){
//             console.log("made it in");
//               _this.setState({ messageLogs : newMessages.existence});
//               let msg = newMessages.existence[0];
//               if (!!msg)
//               _this.createAudioFile(msg);
//           }
//         }
//       });  
//   }

//   createAudioFile (item) {
//     console.log("iteemmm", item);
//     let base = item.messageBinary;
//     this.soundRecordPlayer.playBase(base);
//   }

//   componentWillUnmount() {
//     this._isMounted = false;
//     this.captionsController.unsubscribe();
//   }


//   onPressCall() {
//     this.soundRecordPlayer.onStartRecord({callback: () => {}});
//     this.timer = setInterval(()=> this.sendAudio(), 3000);
//   }


//   // onPressCall() {
//   //   this.soundRecordPlayer.onStartRecord({
//   //     callback: ({recordSecs, recordTime}) => {
//   //       //console.log(recordSecs, recordTime);
//   //     }
//   //   });
//   //   this.timer = setInterval(()=> this.sendAudio(), 3000);
//   // }

//   sendAudio (){
//     this.soundRecordPlayer.onStopRecord({
//       callback: ({recordSecs, recordTime}) => {
//         console.log(recordSecs, recordTime);
//         this.uploadFile();
//         this.soundRecordPlayer.onStartRecord({
//           callback: ({recordSecs}) => {
//             console.log(recordSecs);
//           }
//         });
//       }
//     });
   
//   }

//   onPressHangUp () {
//     this.timer = null;
//     this.soundRecordPlayer.onStopRecord({
//       callback: ({recordSecs}) => {
//         //console.log(recordSecs);
//       }
//     });
//   }

//   quicksendFile = async () => {

//     var RNFS = require('react-native-fs');
//     const path = this.soundRecordPlayer.getAudioFilePath();
//     var content = await RNFS.readFile(path, 'base64');
//     console.log(content);

//     const uploadUrl = 'http://c5d9addc.ngrok.io/call_router';

//     var x = fetch(uploadUrl, {
//       method: 'POST',
//       headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         payload: content,
//         conversationID: "4", 
//         userFrom: USER_FROM,
//         userTo: USER_TO
//       }),
//     });
//   }



//   uploadFile = async () => {

//     var RNFS = require('react-native-fs');
//     const path = this.soundRecordPlayer.getAudioFilePath();
//     var content = await RNFS.readFile(path, 'base64');
//     // console.log(content);

//     const uploadUrl = 'http://c5d9addc.ngrok.io/call_router';

//     var x = fetch(uploadUrl, {
//       method: 'POST',
//       headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         payload: content,
//         conversationID: "4", 
//         userFrom: USER_FROM,
//         userTo: USER_TO
//       }),
//     }).then((response) => console.log(response.json()));
//   }

//   onPressStartRecord = () => {
//     this.soundRecordPlayer.onStartRecord({
//       callback: ({recordSecs, recordTime}) => {
//         this.setState({
//           recordSecs: recordSecs,
//           recordTime: recordTime,
//         });
//       }
//     });
//   }

//   onPressStopPlay = () => {
//     this.soundRecordPlayer.onStopPlay();
//   }

//   onPressStartPlay = () => {
//     this.soundRecordPlayer.onStartPlay({
//       callback : ({currentPositionSec, currentDurationSec, playTime, duration}) => {
//         this.setState({
//           currentPositionSec: currentPositionSec,
//           currentDurationSec: currentDurationSec,
//           playTime: playTime,
//           duration: duration,
//         });
//       }
//     });
//   }

//   onPressStopRecord = () => {
//     this.soundRecordPlayer.onStopRecord({
//       callback : ({recordSecs}) => {
//         this.setState({
//           recordSecs: recordSecs,
//         });
//       }
//     });
//   }




// }



































// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, {Component} from 'react';
// import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList} from 'react-native';
// import SoundRecordPlayer from '../SoundRecordPlayer';
// import styles from '../styles';
// import { restElement } from '@babel/types';
// // import RNEventSource from 'react-native-event-source';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import {USER_TO, USER_FROM} from "../GLOBAL";
// import CaptionsController from '../Controller/CaptionsController';
// // import AudioRecord from 'react-native-audio-record';


// export default class PhoningScreen extends Component {

//   constructor(props){
//     super(props);
//     this.soundRecordPlayer = new SoundRecordPlayer();
//     this.captionsController = new CaptionsController();

//     this.state = {
//       messageLogs: []
//     }

//   }

//   render() {
//     return (
//       <SafeAreaView style={styles.container}>
//         <View style={[styles.row, {padding: 15}]}>
//           <TouchableOpacity
//             style={styles.phoneButton}
//             title="Call"
//             onPress={() => this.onPressStartRecord()}
//             color="green"
//           >
//             <Icon name = "phone" size={50} color="green"/>
//           </TouchableOpacity>
//           <TouchableOpacity
//             style={styles.phoneButton}
//             title="End"
//             onPress={() => this.onPressStopRecord()}
//             color="red"
//           >
//             <Icon name = "phone-hangup" size={50} color="red"/>
//           </TouchableOpacity>
//           {/* <Button
//             title="End"
//             onPress={() => this.onPressStopRecord()}
//             color="red"
//           /> */}
//         </View>
//         <View style={styles.captions}>
//             {this.renderMessages()}
//         </View>
//         <View style={styles.buttonsContainer}>
//           <Button
//             title="play"
//             onPress={() => this.onPressStartPlay()}
//           />
//           <Button
//             title="stop"
//             onPress={() => this.onPressStopPlay()}
//           />
//           <Button
//             title="googly"
//             onPress={() => this.uploadFile()}
//           />
//           {/* <Button
//             title="audio stuff"
//             onPress={() => this.onPressAudioStuff()}
//           /> */}
//         </View>
//       </SafeAreaView>
//     );
//   }


//   // onPressAudioStuff() {
//   //   const options = {
//   //     sampleRate: 16000,  // default 44100
//   //     channels: 1,        // 1 or 2, default 1
//   //     bitsPerSample: 16,  // 8 or 16, default 16
//   //     audioSource: 6,     // android only (see below)
//   //     wavFile: 'test.wav' // default 'audio.wav'
//   //   };
     
//   //   AudioRecord.init(options);
     
//   //   AudioRecord.start();
     
//   //   AudioRecord.stop();
//   //   // or to get the wav file path
//   //   //audioFile = await AudioRecord.stop();
     
//   //   AudioRecord.on('data', data => {
//   //     // base64-encoded audio data chunks
//   //     console.log(data);
//   //   });
//   // }




//   renderMessageItem = ({item}) => {
//     console.log(item);
//     this.createAudioFile(item)
//     return( 
//       <Text style={styles.captionText}>{item.message}</Text>
//       );
//   }
 
//   renderMessages() {
//     return (
//         <FlatList
//           data={this.state.messageLogs}
//           keyExtractor={(item, index) => index.toString()}
//           renderItem={this.renderMessageItem}
//         />
//     );
//   }

//   componentDidMount () {
//     _this = this;
//     this.captionsController.createCaptionsSubscription({ 
//         callback: function (newMessages) {
//             console.log("SHOULD BE ONLY MEssages", newMessages);
//             //if (newMessages.length > 0) {
//               _this.setState({ messageLogs : newMessages.existence});
//            // }
//             // let oldMessages = _this.state.messageLogs;
//             // var combined = oldMessages.concat(newMessages.existence);
//         }
//      });
//   }


//   createAudioFile (item) {
//     console.log("iteemmm", item);
//     let base = item.messageBinary;
//     this.soundRecordPlayer.playBase(base);
//   }


//   componentWillUnmount() {
//       this.captionsController.unsubscribe();
//   }

//   uploadFile = async () => {

//     var RNFS = require('react-native-fs');
//     const path = this.soundRecordPlayer.getAudioFilePath();
//     var content = await RNFS.readFile(path, 'base64');
//     console.log(content);

//     const uploadUrl = 'http://c5d9addc.ngrok.io/call_router';

//     var x = fetch(uploadUrl, {
//       method: 'POST',
//       headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         payload: content,
//         conversationID: "4", 
//         userFrom: USER_FROM,
//         userTo: USER_TO
//       }),
//     }).then((response) => console.log(response.json()));
//   }

//   onPressStartRecord = () => {
//     this.soundRecordPlayer.onStartRecord({
//       callback: ({recordSecs, recordTime}) => {
//         this.setState({
//           recordSecs: recordSecs,
//           recordTime: recordTime,
//         });
//       }
//     });
//   }

//   onPressStopPlay = () => {
//     this.soundRecordPlayer.onStopPlay();
//   }

//   onPressStartPlay = () => {
//     this.soundRecordPlayer.onStartPlay({
//       callback : ({currentPositionSec, currentDurationSec, playTime, duration}) => {
//         this.setState({
//           currentPositionSec: currentPositionSec,
//           currentDurationSec: currentDurationSec,
//           playTime: playTime,
//           duration: duration,
//         });
//       }
//     });
//   }

//   onPressStopRecord = () => {
//     this.soundRecordPlayer.onStopRecord({
//       callback : ({recordSecs}) => {
//         this.setState({
//           recordSecs: recordSecs,
//         });
//       }
//     });
//   }




// }

































































// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, {Component} from 'react';
// import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList} from 'react-native';
// import SoundRecordPlayer from '../SoundRecordPlayer';
// import styles from '../styles';

// export default class PhoningScreen extends Component {

//   constructor(props){
//     super(props);
//     this.soundRecordPlayer = new SoundRecordPlayer();
//     console.log(this.soundRecordPlayer);
//   }

//   render() {
//     return (
//       <SafeAreaView style={styles.container}>
//         <View style={styles.buttonsContainer}>
//           <Button
//             title="start"
//             onPress={() => this.onPressStartRecord()}
//           />
//           <Button
//             title="cancel"
//             onPress={() => this.onPressStopRecord()}
//           />
//           <Button
//             title="play"
//             onPress={() => this.onPressStartPlay()}
//           />
//           <Button
//             title="stop"
//             onPress={() => this.onPressStopPlay()}
//           />
//           <Button
//             title="googly"
//             onPress={() => this.uploadFile()}
//           />
//         </View>
//       </SafeAreaView>
//     );
//   }

//   uploadFile = async () => {

//     var RNFS = require('react-native-fs');
//     const path = this.soundRecordPlayer.getAudioFilePath();
//     var content = await RNFS.readFile(path, 'base64');
//     console.log(content);

//     const uploadUrl = 'http://localhost:5000/call_router';

//     var x = fetch(uploadUrl, {
//       method: 'POST',
//       headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         payload: content,
//         conversationID: "0", 
//         userFrom: '1',
//         userTo: '2'
//       }),
//     }).then((response) => console.log(response.json()));
//   }

//   onPressStartRecord = () => {
//     this.soundRecordPlayer.onStartRecord({
//       callback: ({recordSecs, recordTime}) => {
//         this.setState({
//           recordSecs: recordSecs,
//           recordTime: recordTime,
//         });
//       }
//     });
//   }

//   onPressStopPlay = () => {
//     this.soundRecordPlayer.onStopPlay();
//   }

//   onPressStartPlay = () => {
//     this.soundRecordPlayer.onStartPlay({
//       callback : ({currentPositionSec, currentDurationSec, playTime, duration}) => {
//         this.setState({
//           currentPositionSec: currentPositionSec,
//           currentDurationSec: currentDurationSec,
//           playTime: playTime,
//           duration: duration,
//         });
//       }
//     });
//   }

//   onPressStopRecord = () => {
//     this.soundRecordPlayer.onStopRecord({
//       callback : ({recordSecs}) => {
//         this.setState({
//           recordSecs: recordSecs,
//         });
//       }
//     });
//   }
  


// }


















































