/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList} from 'react-native';
import SoundRecordPlayer from '../SoundRecordPlayer';
import styles from '../styles';
import ChatBubble from '../Components/ChatBubble';
import { LabeledPicker } from '../Components/LabeledPicker';
// import DropdownButton from 'react-bootstrap/DropdownButton'

const languages = [
  {
    label: "English",
    value: "en"
  },
  {
    label: "French",
    value: "fr"
  },
  {
    label: "Spanish",
    value: "es"

  },
  {
    label: "German",
    value: "de"
  },
  {
    label: "Chinese (Simplified)",
    value: "zh-cn"
  },
  {
    label: "Arabic",
    value: "ar"
  }
   
]


export default class ChatScreen extends Component {

  constructor(props){
    super(props);
    this.arrayOfMessages = this.props.navigation.state.params.arrayOfMessages;
    this.soundRecordPlayer = new SoundRecordPlayer();
    this.conversationID = this.arrayOfMessages[0].conversationID;

    this.state = {
      selectedLanguage : languages[0],
      messages: this.arrayOfMessages
    }
  }

  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: navigation.state.params.arrayOfMessages[0].conversationID,
    }
  }


  renderChatBubble = ({item}) => {
    console.log(item);
    return( 
      <ChatBubble
        convoItem={item}
      />
      );
  }

  renderHeader() {
      console.log(this.convos);
      return (
        <View style= {{flex:1}}>
          <LabeledPicker
            label='Language'
            placeholderText="Select Language for Translation"
            items={languages}
            value={this.state.selectedLanguage}
            onValueChange={(value) => this.setState({selectedLanguage: value})}
            onDonePress={() => this.getNewLanguage()}
          />
          {/* <Text style={styles.chatScreenHeader}>{this.conversationID}</Text> */}
        </View>
      )
  }

  getNewLanguage() {
      let languageCode = this.state.selectedLanguage;
      

      if (languageCode != null) {
        console.log("language:", languageCode);
      }

      _this = this;
      if (languageCode != null) {
          fetch(`http://48ff42d1.ngrok.io/translate_message?language=${languageCode}&conversationID=${this.conversationID}`)
          .then(result => result.json())
          .then(result => {
            console.log("databade result", result);
            _this.setState({
              messages: result.conversationHistory
            });
            //this.captionsLogs = result;
            
        });
      }
    
  }
 
  render() {
    console.log("rendering messages", this.state.messages);
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={this.state.messages}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderChatBubble}
          ListHeaderComponent={this.renderHeader()}
        />
      </SafeAreaView>
    );
  }



}









// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, {Component} from 'react';
// import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList} from 'react-native';
// import SoundRecordPlayer from '../SoundRecordPlayer';
// import styles from '../styles';
// import ChatBubble from '../Components/ChatBubble';
// import CallHistoryController from '../Controller/CallHistoryController';

// export default class ChatScreen extends Component {

//   constructor(props){
//     super(props);
//     this.arrayOfMessages = this.props.navigation.state.params.arrayOfMessages;
//     this.soundRecordPlayer = new SoundRecordPlayer();
//     this.conversationID = this.arrayOfMessages[0].conversationID;
//     this.callHistoryController = new CallHistoryController();

//     this.state = {
//       messages : this.arrayOfMessages
//     }

//   }

//   componentDidMount(){
//     this.load()
//     this.props.navigation.addListener('willFocus', this.load)
//   }
//   load = () => {
//     _this = this;
//     this.callHistoryController.getChatHistory({
//         callback: function(newCallHistoryLogs) {
//               console.log("here pls in chat", newCallHistoryLogs);
//               _this.setState({ messages : newCallHistoryLogs[this.conversationID]});
//           }
//       });
//   }

//   renderChatBubble = ({item}) => {
//     console.log(item);
//     return( 
//       <ChatBubble
//         convoItem={item}
//       />
//       );
//   }

//   renderHeader() {
//       console.log(this.convos);
//       return (
//           <Text style={styles.chatScreenHeader}>{this.conversationID}</Text>
//       )
//   }
 
//   render() {
//     return (
//       <SafeAreaView style={styles.container}>
//         <FlatList
//           data={this.state.messages}
//           keyExtractor={(item, index) => index.toString()}
//           renderItem={this.renderChatBubble}
//           ListHeaderComponent={this.renderHeader()}
//         />
//       </SafeAreaView>
//     );
//   }



// }

































































