/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, Text, View, Button, SafeAreaView, ScrollView, FlatList} from 'react-native';
import {ListItem} from 'react-native-elements'
import styles from '../styles';
import CallHistoryController from '../Controller/CallHistoryController';

export default class ConvoListScreen extends Component {

  constructor(props){
    super(props);
    this.callHistoryController = new CallHistoryController();

    this.state = {
      callLogs: []
    }
  }

  componentDidMount(){
    this.load()
    this.props.navigation.addListener('willFocus', this.load)
  }
  load = () => {
    _this = this;
    this.callHistoryController.getChatHistory({
        callback: function(newCallHistoryLogs) {
              console.log("here pls", newCallHistoryLogs);
              _this.setState({ callLogs : newCallHistoryLogs});
          }
      });
  }

  // componentWillMount () {
  //   _this = this;
  //   this.callHistoryController.createChatHistorySubscription({ 
  //       callback: function (newCallHistoryLogs) {
  //           console.log("SHOULD BE ONLY", newCallHistoryLogs);
  //           _this.setState({ callLogs : newCallHistoryLogs});
  //           _this.render();
  //       }
  //    });
  //    console.log("componenet will mount!");
  // }



  // componentWillUnmount() {
  //     this.callHistoryController.unsubscribe();
  // }

  onPressConvoItem(arrayOfMessages) {
      console.log(arrayOfMessages);
      this.props.navigation.navigate("ChatScreen", {arrayOfMessages});
  }

  renderItem = ({item}) => {
    console.log("loookoy", item);
    return (
      <ListItem
        containerStyle={styles.conversationItem}
        title={
            <View style={styles.row}>
                <Text>{item[0].conversationID}</Text>
                <Text>{new Date(item[0].time).toLocaleString()}</Text>
            </View>
        }
        chevron
        onPress = {() => this.onPressConvoItem(item)} 
    />
    );
  }
    
  render() {

    const conversationItems = this.state.callLogs;
    const convos = Object.values(conversationItems);
    console.log("rendering convo list", conversationItems);
    console.log("rendering convo list convos", convos);
    console.disableYellowBox=true;
    
    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={convos}
                renderItem={this.renderItem}
                />
        </SafeAreaView>

    )
  }
  
 
  


}



















