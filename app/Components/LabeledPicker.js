import styles from "../styles";
import React, { Component } from "react";
import { Text, View, StyleSheet} from "react-native";
import PickerSelect from 'react-native-picker-select';


export class LabeledPicker extends Component {
    
    constructor(props) {
        super(props);
        
        if (!!this.props.placeholderText){
            this.placeholderCustom = {
                label: this.props.placeholderText,
                value: null,
                color: 'grey'
            };
        } else {
            this.placeholderCustom = {}
        }
    
        this.label = (!!this.props.label) ? this.props.label : '';
        this.placeholderColor = (!!this.props.placeholderColor) ? this.props.placeholderColor : 'grey'
    }
    
    render() {
        return (
            <View style={[styles.container, {backgroundColor: this.props.backgroundColor}]}>
                <Text style={{paddingLeft:15, paddingTop:15}}>
                    {this.label}
                </Text>
                <PickerSelect
                    placeholder={this.placeholderCustom}
                    placeholderTextColor={this.placeholderColor}
                    items={this.props.items}
                    value={this.props.value}
                    onValueChange={this.props.onValueChange}
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                    onUpArrow={this.props.onUpArrow}
                    onDownArrow={this.props.onDownArrow}
                    onDonePress={(!!this.props.onDonePress) ? this.props.onDonePress : undefined}
                    //value={this.state.selectedTransferType}
                />
            </View>
        );
    }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        height: 40,
        flex:1,
        //fontSize: 16,
        paddingHorizontal: 10,
        borderWidth: 1,
        //marginBottom: 15,
        marginTop: 2,
        marginLeft: 15,
        marginRight: 15, 
        borderColor: 'grey',
        borderRadius: 5,
        backgroundColor: 'white',
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        height: 40,
        flex:1,
        //fontSize: 16,
        marginTop: 2,
        marginLeft: 15,
        marginRight: 15, 
        //marginBottom: 15,
        paddingHorizontal: 10,
        //borderWidth: 0.5,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 8,
        backgroundColor: 'white',
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});

