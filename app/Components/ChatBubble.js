
import React, {Component} from 'react';
import {Platform, Text, View, Button} from 'react-native';
import styles from '../styles';
import {USER_FROM, USER_TO} from '../GLOBAL';

/**
 * @param {bubbleColor} String like 'blue' or 'white'
 * @param {message} String text message of chat
 */


export default class App extends Component {

    constructor(props){
      super(props);
    //   this.time = this.props.time;
    //   this.message = this.props.message;

      
      this.convoItem = this.props.convoItem;
      this.messageIsFromMe = this.convoItem.user_from == USER_FROM;

      this.time = new Date(this.convoItem.time).toLocaleString();
      
      this.textColor = (this.messageIsFromMe) ? 'white' : 'black';
      this.timeAlign = (this.messageIsFromMe) ? 'right' : 'left';
      this.bubbleColor = (this.messageIsFromMe) ? 'blue' : '#DCDCDC';
      this.bubbleAlign = (this.messageIsFromMe) ? 'flex-end' : 'flex-start';
    }

    render () {

        this.convoItem = this.props.convoItem;
        this.message = (!!this.convoItem.message.translatedMessage) ? this.convoItem.message.translatedMessage : this.convoItem.message;
        console.log("JAMIE",this.convoItem);

        return (
            <View style={[styles.messageBubble, {alignItems: this.bubbleAlign}]}>
                <Text style={[styles.timeStamp, {textAlign: this.timeAlign}]}>{this.time}</Text>
                <View style={[
                    styles.bubble, 
                    {backgroundColor: this.bubbleColor}
                    ]}>
                    <Text style={{color: this.textColor}}>{this.message}</Text>
                </View>
            </View>
        );
        
    }
}
    