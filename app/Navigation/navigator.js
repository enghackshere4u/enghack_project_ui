
import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import ChatScreen from '../Screens/ChatScreen';
import PhoningScreen from '../Screens/PhoningScreen';
import ConvoListScreen from '../Screens/ConvoListScreen';

const chatNavigator = createStackNavigator({
    ConvoListScreen,
    ChatScreen,
})

const TabNavigator = createBottomTabNavigator({
    Phone: PhoningScreen,
    History: chatNavigator,
  
   
});
const Navigator = createAppContainer(TabNavigator);
export default Navigator;