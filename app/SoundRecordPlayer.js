import AudioRecorderPlayer from 'react-native-audio-recorder-player';
import Sound from 'react-native-sound';

export default class SoundRecordPlayer {

    constructor(){
      this.audioRecorderPlayer = new AudioRecorderPlayer();
      console.log(this.audioRecorderPlayer);
      this.audioPathFile = null;
    }

    getAudioFilePath () {
      return this.audioPathFile;
    }

    playBase (base64Str) {
      console.log ("in play base");
      var RNFS = require('react-native-fs');
      //var path = this.getAudioFilePath();
      var path = RNFS.DocumentDirectoryPath + '/test.txt';

      RNFS.writeFile(path, base64Str, 'base64')
        .then(() => playSound())
        .catch((err) => {
          console.log(err.message);
        });

      const playSound = () => {
        console.log("about to paly sth");
        const sound = new Sound(path, '', (error) => {
          if (error) { 
            console.log (error) 
          }
          sound.play();
        });
      }  
    }

  onStartRecord = async () => {
      this.audioPathFile = await this.audioRecorderPlayer.startRecorder();
  }
      
  // onStopRecord = async () => {
  //   const result = await this.audioRecorderPlayer.stopRecorder();
  // }

  onStopRecord = async ({callback}) => {
    const result = await this.audioRecorderPlayer.stopRecorder();
    //this.audioRecorderPlayer.removeRecordBackListener();
    callback({});
  }


    //   const url = 'http://sample.com/sample.mp3' ;
    //   AudioPlayer.prepare(url, () => {
    //     AudioPlayer.play();
    //   }
        
    // }

    // playBase (base64Str) {
    //   // require the module
    //   var RNFS = require('react-native-fs');
    //   // create a path you want to write to
    //   var path = this.getAudioFilePath();
 
    //   // write the file
    //   RNFS.writeFile(path, base64Str, 'base64')
    //   .then(() => playSound())
    //     .catch((err) => {
    //       console.log(err.message);
    //     });

    //   const playSound = () => {
    //     const sound = new Sound(path, '', () => callback(sound))
    //   }
    //   const callback = () => sound.play(successCallback)
        
    // }


        
      onStartPlay = async ({callback}) => {
        console.log('onStartPlay');
        const msg = await this.audioRecorderPlayer.startPlayer();
        console.log(msg);
        this.audioRecorderPlayer.addPlayBackListener((e) => {
          if (e.current_position === e.duration) {
            console.log('finished');
            this.audioRecorderPlayer.stopPlayer();
          }
          callback({
            currentPositionSec: e.current_position,
            currentDurationSec: e.duration,
            playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
            duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
          });
          
          return;
        });
      }
        
      onPausePlay = async () => {
        await this.audioRecorderPlayer.pausePlayer();
      }
        
      onStopPlay = async () => {
        console.log('onStopPlay');
        this.audioRecorderPlayer.stopPlayer();
        this.audioRecorderPlayer.removePlayBackListener();
      }

}



















// import AudioRecorderPlayer from 'react-native-audio-recorder-player';
// import Sound from 'react-native-sound';

// export default class SoundRecordPlayer {

//     constructor(){
//       this.audioRecorderPlayer = new AudioRecorderPlayer();
//       console.log(this.audioRecorderPlayer);
//       this.audioPathFile = null;
//     }

//     getAudioFilePath () {
//       return this.audioPathFile;
//     }

//     playBase (base64Str) {
//       console.log ("in play base");
//       var RNFS = require('react-native-fs');
//       //var path = this.getAudioFilePath();
//       var path = RNFS.DocumentDirectoryPath + '/test.txt';

//       RNFS.writeFile(path, base64Str, 'base64')
//         .then(() => playSound())
//         .catch((err) => {
//           console.log(err.message);
//         });

//       const playSound = () => {
//         console.log("about to paly sth");
//         const sound = new Sound(path, '', (error) => {
//           if (error) { 
//             console.log (error) 
//           }
//           sound.play();
//         });
//       }  
//     }


//     //   const url = 'http://sample.com/sample.mp3' ;
//     //   AudioPlayer.prepare(url, () => {
//     //     AudioPlayer.play();
//     //   }
        
//     // }

//     // playBase (base64Str) {
//     //   // require the module
//     //   var RNFS = require('react-native-fs');
//     //   // create a path you want to write to
//     //   var path = this.getAudioFilePath();
 
//     //   // write the file
//     //   RNFS.writeFile(path, base64Str, 'base64')
//     //   .then(() => playSound())
//     //     .catch((err) => {
//     //       console.log(err.message);
//     //     });

//     //   const playSound = () => {
//     //     const sound = new Sound(path, '', () => callback(sound))
//     //   }
//     //   const callback = () => sound.play(successCallback)
        
//     // }

//     onStartRecord = async ({callback}) => {
//         //const result = await this.audioRecorderPlayer.startRecorder(path);
//         this.audioPathFile = await this.audioRecorderPlayer.startRecorder();
//         this.audioRecorderPlayer.addRecordBackListener((e) => {
//           callback({
//             recordSecs: e.current_position, 
//             recordTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position))
//           })
//           return;
//         });
//         console.log(this.audioPathFile);
//       }
        
//       onStopRecord = async ({callback}) => {
//         const result = await this.audioRecorderPlayer.stopRecorder();
//         this.audioRecorderPlayer.removeRecordBackListener();
        
//         callback({
//           recordSecs:0,
//         })
        
//         console.log(result);
//       }
        
//       onStartPlay = async ({callback}) => {
//         console.log('onStartPlay');
//         const msg = await this.audioRecorderPlayer.startPlayer();
//         console.log(msg);
//         this.audioRecorderPlayer.addPlayBackListener((e) => {
//           if (e.current_position === e.duration) {
//             console.log('finished');
//             this.audioRecorderPlayer.stopPlayer();
//           }
//           callback({
//             currentPositionSec: e.current_position,
//             currentDurationSec: e.duration,
//             playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
//             duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
//           });
          
//           return;
//         });
//       }
        
//       onPausePlay = async () => {
//         await this.audioRecorderPlayer.pausePlayer();
//       }
        
//       onStopPlay = async () => {
//         console.log('onStopPlay');
//         this.audioRecorderPlayer.stopPlayer();
//         this.audioRecorderPlayer.removePlayBackListener();
//       }

// }


