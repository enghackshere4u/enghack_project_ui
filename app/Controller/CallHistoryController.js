
export default class CallHistoryController
{
	constructor() 
	{
        this.callHistoryLogs = {};
    };

	// createChatHistorySubscription({ callback }) // Whenever the rateservice updates its subscription, i.e. the listings, then this will fire and update the watchlist
	// {
    //     this.timer = setInterval(()=> this.getChatHistory({ callback }), 1000);
	// };

    getChatHistory ({callback}) {
        return fetch('http://48ff42d1.ngrok.io/conversation_history')
        .then(result => result.json())
        .then(result => {
            //this.callHistoryLogs = result;
            let modifiedResult = this.saveLogs(result);
            callback(modifiedResult);
        });
    }

    // getNewLanguage({languageCode, callback}) {
    //     if (languageCode != null) {
    //         console.log("language:", languageCode);
    //     }
    
    //     _this = this;
    //     if (languageCode != null) {
    //     return fetch(`http://localhost:5000/translate_message?language=${languageCode}&conversationID=${this.conversationID}`)
    //         .then(result => result.json())
    //         .then(result => {
    //             console.log(result);
    //             this.saveLogs(result);
    //             callback(result);
    //         });
    //     }
    // }

    unsubscribe() {
        this.timer = null; // here...
    }


    saveLogs(newCallLogs) {
        console.log(newCallLogs);
        const arrayOfObjects = newCallLogs.conversationHistory;
        console.log("convo history", arrayOfObjects);

        this.callHistoryLogs = newCallLogs;

        const groupBy = key => array => 
        array.reduce((objectsByKeyValue, obj) => {
          const value = obj[key];
          objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
          return objectsByKeyValue;
        }, {});
      
        const groupByConversationID = groupBy('conversationID');
        const conversationsById = groupByConversationID(arrayOfObjects);
        console.log("conversationsById", conversationsById);

           //sort the all of the conversation messages
        function compareConvos(convo1, convo2) {
            const time1 = new Date(convo1.time);
            const time2 = new Date (convo2.time);
    
            if (time1 < time2) {
            return -1;
            }
            
            if (time1 > time2) {
            return 1;
            }
            return 0;
        }

        for (var conversationID in conversationsById) {
            conversationsById[conversationID].sort(compareConvos);
        }

        console.log("final product?", conversationsById);
        this.callHistoryLogs = conversationsById;
        return this.callHistoryLogs;
    }

}

