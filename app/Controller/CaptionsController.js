
import {USER_FROM} from "../GLOBAL";

export default class CaptionsController
{
	constructor() 
	{
        this.callHistoryLogs = {};
    };

	createCaptionsSubscription({ language, callback }) // Whenever the rateservice updates its subscription, i.e. the listings, then this will fire and update the watchlist
	{
        this.timer = setInterval(()=> this.getCaptions({ language, callback }), 1000);
	};

    getCaptions ({ language, callback }) {
        fetch(`http://48ff42d1.ngrok.io/get_message?language=${language}&userTo=${USER_FROM}`)
        .then(result => result.json())
        .then(result => {
            //console.log(result);
            //this.captionsLogs = result;
            //this.saveLogs(result);
            callback(result);
        });
    }

    unsubscribe() {
        this.timer = null; // here...
    }

    // saveLogs(newCallLogs) {
    //     console.log(newCallLogs);
    //     const arrayOfObjects = newCallLogs.existence;
    //     console.log("convo history", arrayOfObjects);

        // this.callHistoryLogs = newCallLogs;

        // const groupBy = key => array => 
        // array.reduce((objectsByKeyValue, obj) => {
        //   const value = obj[key];
        //   objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
        //   return objectsByKeyValue;
        // }, {});
      
        // const groupByConversationID = groupBy('conversationID');
        // const conversationsById = groupByConversationID(arrayOfObjects);
        // console.log("conversationsById", conversationsById);

        //    //sort the all of the conversation messages
        // function compareConvos(convo1, convo2) {
        //     const time1 = new Date(convo1.time);
        //     const time2 = new Date (convo2.time);
    
        //     if (time1 < time2) {
        //     return -1;
        //     }
            
        //     if (time1 > time2) {
        //     return 1;
        //     }
        //     return 0;
        // }

        // for (var conversationID in conversationsById) {
        //     conversationsById[conversationID].sort(compareConvos);
        // }

        // console.log("final product?", conversationsById);
        // this.callHistoryLogs = conversationsById;

    // }


    // saveLogs(newMessages) {
    //     console.log(newMessages);


    //     // const arrayOfObjects = newCallLogs.conversationHistory;
    //     // console.log("convo history", arrayOfObjects);

    //     // this.captionsLogs = newCallLogs;

    //     // const groupBy = key => array => 
    //     // array.reduce((objectsByKeyValue, obj) => {
    //     //   const value = obj[key];
    //     //   objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
    //     //   return objectsByKeyValue;
    //     // }, {});
      
    //     // const groupByConversationID = groupBy('conversationID');
    //     // const conversationsById = groupByConversationID(arrayOfObjects);
    //     // console.log("conversationsById", conversationsById);

    //     //    //sort the all of the conversation messages
    //     // function compareConvos(convo1, convo2) {
    //     //     const time1 = new Date(convo1.time);
    //     //     const time2 = new Date (convo2.time);
    
    //     //     if (time1 < time2) {
    //     //     return -1;
    //     //     }
            
    //     //     if (time1 > time2) {
    //     //     return 1;
    //     //     }
    //     //     return 0;
    //     // }

    //     // for (var conversationID in conversationsById) {
    //     //     conversationsById[conversationID].sort(compareConvos);
    //     // }

    //     // console.log("final product?", conversationsById);
    //     // this.captionsLogs = conversationsById;

    // }

}

